import { FavoritesBtn } from "../FavoritesBtn/FavoritesBtn";
import { CartBtn } from "../CartBtn/CartBtn";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

export function Header() {
  const favItems = useSelector((state) => state.favItems);
  const cartItems = useSelector((state) => state.cartItems);

  return (
    <>
      <nav style={{ zIndex: 1 }} className="navbar bg-light border-bottom sticky-top">
        <div className="container">
          <Link className="btn btn-light fs-4 pb-2" to={"/"}>
            Laptops
          </Link>
          <div className="d-flex gap-4">
            <Link to={"/favorites"}>
              <FavoritesBtn favItemsCount={favItems.length} />
            </Link>
            <Link to={"/cart"}>
              <CartBtn cartItemsCount={cartItems.length} />
            </Link>
          </div>
        </div>
      </nav>
    </>
  );
}
