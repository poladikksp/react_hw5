import { ItemCard } from "../ItemCard/ItemCard";
import { useSelector } from "react-redux";

export function ItemList({ isFav, isCart }) {
  const items = useSelector((state) => state.items);
  const favItems = useSelector((state) => state.favItems);
  const cartItems = useSelector((state) => state.cartItems);

  return (
    <>
      {(isFav && favItems.length === 0) || (isCart && cartItems.length === 0) ? (
        <p className="w-100 text-center fs-4">Empty...</p>
      ) : null}
      <ul className="d-flex justify-content-center mt-3 p-0 flex-wrap gap-3">
        {isCart
          ? cartItems.map((item, index) => {
              return <ItemCard inCart={true} key={index} item={item} />;
            })
          : null}
        {isFav
          ? favItems.map((item) => {
              return <ItemCard favorite={true} key={item.id} item={item} />;
            })
          : null}
        {!isFav && !isCart
          ? items.map((item) => {
              return (
                <ItemCard
                  favorite={favItems.find((favItem) => favItem.id === item.id) ? true : false}
                  key={item.id}
                  item={item}
                />
              );
            })
          : null}
      </ul>
    </>
  );
}
