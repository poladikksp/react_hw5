import { ItemList } from "../components/ItemsList/ItemsList";
import { CheckoutForm } from "../components/CheckoutFrom/CheckoutForm";
import { useSelector } from "react-redux";

export function Cart() {
  const cartItems = useSelector((state) => state.cartItems);

  return (
    <>
      <h3 className="text-center pt-3 pb-2">Your cart:</h3>
      <div className="d-flex align-items-center align-items-sm-start flex-column flex-sm-row justify-content-between">
        <ItemList isCart={true} />
        {cartItems.length !== 0 && <CheckoutForm />}
      </div>
    </>
  );
}
