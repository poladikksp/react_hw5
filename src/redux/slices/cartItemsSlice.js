import { createSlice } from "@reduxjs/toolkit";

const cartItemsSlice = createSlice({
  name: "favItems",
  initialState: [],
  reducers: {
    setCartItems: (state, action) => {
      return action.payload;
    },
    addToCart: (state, action) => {
      state.push(action.payload);
    },
    removeFromCart: (state, action) => {
      const deletingElIndex = state.findIndex((item) => item.id === action.payload.id);
      const newCartArr = state.filter((item, index) => index !== deletingElIndex);
      return newCartArr;
    },
  },
});

export const { setCartItems, addToCart, removeFromCart } = cartItemsSlice.actions;

export default cartItemsSlice.reducer;
