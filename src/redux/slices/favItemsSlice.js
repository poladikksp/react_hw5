import { createSlice } from "@reduxjs/toolkit";

const favItemsSlice = createSlice({
  name: "favItems",
  initialState: [],
  reducers: {
    setFavItems: (state, action) => {
      return action.payload;
    },
    addToFav: (state, action) => {
      state.push(action.payload);
    },
    removeFromFav: (state, action) => {
      const newFavArr = state.filter((item) => item.id !== action.payload);
      return newFavArr;
    },
  },
});

export const { setFavItems, addToFav, removeFromFav } = favItemsSlice.actions;

export default favItemsSlice.reducer;
